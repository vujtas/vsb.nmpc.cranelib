﻿
using CenterSpace.NMath.Analysis;
using CenterSpace.NMath.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConstraintType = CenterSpace.NMath.Analysis.ConstraintType;
using NonlinearConstraint = CenterSpace.NMath.Analysis.NonlinearConstraint;

namespace VSB.NMPC.CraneLib
{
    public class NMMathSolver:ISolver
    {
        private MathMethods mathMethods;
        int N;
        double[] x1;
        public NMMathSolver(int N, double[] x1)
        {
            this.N = N;
            this.x1 = x1;
            mathMethods = new MathMethods(N, x1);
        }

        private DoubleVector ConstraintParams(double[] equalityValues,double[] xg)
        {
            double[] result = mathMethods.BoxEqualityFunction(equalityValues, xg);
            return new DoubleVector(result);
        }

        public List<Tuple<double[], double>> SolveBox(double[] equalityValues,int iteration)
        {
            double[] xg = new double[] { 0, 0, 0, 0 };
            List<Tuple<double[], double>> result = new List<Tuple<double[], double>>();
            MyObjectiveFunction myFunction = new MyObjectiveFunction(N, x1, equalityValues);
            DoubleFunctional objective = myFunction;




            for (int i = 0; i < iteration; i++)
            {
                var constraints = new List<Constraint>();
                /*var c1 =
                    new DoubleFunctionalDelegate(2, new Func<DoubleVector,
                        double>(delegate (DoubleVector v) { return v[0]; }));
                */
               /* var constraint1 = new NonlinearConstraint(
                    c1, ConstraintType.GreaterThanOrEqualTo);
               */
                var linearConstraint = new LinearConstraint(ConstraintParams(equalityValues, xg), 4, ConstraintType.EqualTo, 0.05);
                constraints.Add(linearConstraint);
                var problem =
                new NonlinearProgrammingProblem(objective, constraints);
                var solver = new StochasticHillClimbingSolver();
                var solverParams = new StochasticHillClimbingParameters
                {
                    TimeLimitMilliSeconds = 10000,
                    Presolve = true
                };
                solver.Solve(problem);
                double diff = (double)solver.Result;
                result.Add(new Tuple<double[], double>(xg, diff));
                xg = mathMethods.RungeKutta(xg, diff, diff);
            }
            return result;
        }

        public List<Tuple<double[], double>> CreateSolution(double[] equalityValues)
        {
            return SolveBox(equalityValues, 100);
        }

        private class MyObjectiveFunction : DoubleFunctional
        {
            private int N;
            private double[] x1;
            private double[] equalityValues;
            // Constructor. Must initilialize the base class with the 
            // dimension of the domain--2 in this case.
            public MyObjectiveFunction(int N,double[] x1,double[] equalityValues)
              : base(4)
            {
                this.N = N;
                this.x1 = x1;
                this.equalityValues = equalityValues;
            }

            public override double Evaluate(DoubleVector x)
            {
                MathMethods nmath = new MathMethods(N, x1);        
                return nmath.BoxObjectiveFunction(equalityValues);
            }

        }


    }
}
