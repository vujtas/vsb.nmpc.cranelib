﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VSB.NMPC.CraneLib
{
    public interface ISolver
    {
         List<Tuple<double[], double>> CreateSolution(double[] equalityValues);
    }
}
