﻿using Accord.Math.Optimization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VSB.NMPC.CraneLib
{
    public class Crane:ISolver
    {

        private MathMethods mathMethods;
        
        public Crane(int N,double[] x1)
        {
            mathMethods = new MathMethods(N, x1);
        }


       

        private List<Tuple<double[],double>> SolveBox(double[] equalityValues, int iteration)
        {
            double[] xg = new double[] { 0, 0, 0, 0 };
            List<Tuple<double[], double>> result = new List<Tuple<double[], double>>();
            Func<double[], double> fun = mathMethods.BoxObjectiveFunction;
            Accord.Math.Optimization.NonlinearObjectiveFunction function = new Accord.Math.Optimization.NonlinearObjectiveFunction(4, 
                fun);
            for(int i=0;i<iteration;i++)
            {
                IConstraint constraint = new LinearConstraint(mathMethods.BoxEqualityFunction(equalityValues, xg));
                Accord.Math.Optimization.AugmentedLagrangian l = new AugmentedLagrangian(function, new IConstraint[] { constraint });
                double diff = l.Solution[0];
                result.Add(new Tuple<double[], double>(xg, diff));
                xg = mathMethods.RungeKutta(xg, diff, diff);
            }
                      
            return result;    
        }

        /// <summary>
        /// Returns as first param array of state variables, as second param returns array of control variable
        /// </summary>
        /// <param name="equalityValues"></param>
        /// <returns></returns>
        public List<Tuple<double[], double>> CreateSolution(double[] equalityValues)
        {
            return SolveBox(equalityValues,100);
        }


    }

}
