﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VSB.NMPC.CraneLib
{
    public class MathMethods
    {
        private readonly int N;

        private readonly double[] x1;
        public MathMethods(int N, double[] x1)
        {
            this.N = N;
            this.x1 = x1;
        }


        public double[] RungeKutta(double[] x_cur, double u_next, double[] u_cur)
        {
            List<double> result = new List<double>();
            for (int i = 0; i < u_cur.Length; i++)
            {
                var subResult = RungeKutta(x_cur, u_next, u_cur[i]).ToList();
                result.AddRange(subResult);
            }
            return result.ToArray();
        }

        public double[] RungeKutta(double[] x_cur, double u_next, double u_cur)
        {
            double h = 0.25;
            double[] xj = new double[] { 0, 0, 0, 0 };
            var up = (u_next + u_cur) * 0.5;
            var k11 = x_cur[1];
            var k12 = u_cur - x_cur[1];
            var k13 = x_cur[3];
            var k14 = 0.0004 * (-4905 * Math.Sin(x_cur[2]) - 500 * x_cur[3] + 5000 * (u_cur - x_cur[1]) * Math.Cos(x_cur[2]));
            var x2p = x_cur[1] + k12 * h * 0.5;
            var x3p = x_cur[2] + k13 * h * 0.5;
            var x4p = x_cur[3] + k14 * h * 0.5;
            var k21 = x2p;
            var k22 = up - x2p;
            k22 = 10 * k22;
            var k23 = x4p;
            var k24 = 0.0004 * (-4905 * Math.Sin(x3p) - 500 * x4p + 5000 * (up - x2p) * Math.Cos(x3p));

            var x2q = x_cur[1] + k22 * h * 0.5;
            var x3q = x_cur[2] + k23 * h * 0.5;
            var x4q = x_cur[3] + k24 * h * 0.5;


            var k31 = x2q;
            var k32 = up - x2q;
            k32 = 10 * k32;
            var k33 = x4q;
            var k34 = 0.0004 * (-4905 * Math.Sin(x3q) - 500 * x4q + 5000 * (up - x2q) * Math.Cos(x3q));


            var x2e = x_cur[1] + k32 * h;
            var x3e = x_cur[2] + k33 * h;
            var x4e = x_cur[3] + k34 * h;


            var k41 = x2e;
            var k42 = u_next - x2e;
             k42 = 10 * k42;
            var k43 = x4e;
            var k44 = 0.0004 * (-4905 * Math.Sin(x3e) - 500 * x4e + 5000 * (u_next - x2e) * Math.Cos(x3e));


            xj[0] = x_cur[0] + h * (k11 + 2.0 * k21 + 2.0 * k31 + k41) / 6.0;
            xj[1] = x_cur[1] + h * (k12 + 2.0 * k22 + 2.0 * k32 + k42) / 6.0;
            xj[2] = x_cur[2] + h * (k13 + 2.0 * k23 + 2.0 * k33 + k43) / 6.0;
            xj[3] = x_cur[3] + h * (k14 + 2.0 * k24 + 2.0 * k34 + k44) / 6.0;
            return xj;
        }

        public List<double> CustomJ(double[] x, double[] h)
        {
            List<double> result = new List<double>();
            if (x.Length != h.Length)
                throw new Exception("Velikost vektoru je rozdílná");
            for (int i = 0; i < x.Length; i++)
            {
                result.Add((x[i] - h[i]) * (x[i] - h[i]));
            }
            return result;
        }
        public List<double> CustomSum(double[] x, double[] h)
        {
            List<double> result = new List<double>();
            if (x.Length != h.Length)
                throw new Exception("Velikost vektoru je rozdílná");
            for (int i = 0; i < x.Length; i++)
            {
                result.Add(x[i] + h[i]);
            }
            return result;
        }

        public List<double> CustomSub(double[] x, double[] h)
        {
            List<double> result = new List<double>();
            if (x.Length != h.Length)
                throw new Exception("Velikost vektoru je rozdílná");
            for (int i = 0; i < x.Length; i++)
            {
                result.Add(x[i] - h[i]);
            }
            return result;
        }
        public List<double> CustomMult(double[] x, double[] h)
        {
            List<double> result = new List<double>();
            if (x.Length != h.Length)
                throw new Exception("Velikost vektoru je rozdílná");
            for (int i = 0; i < x.Length; i++)
            {
                result.Add(x[i] * h[i]);
            }
            return result;
        }



        public double Sum(double[] arr)
        {
            return arr.Sum();
        }



        public double[] CustomReshape(double[] h1, double[] h2, double[] h3, double[] h4)
        {
            List<double> result = new List<double>();
            foreach (var x in h1)
                result.Add(x);
            foreach (var x in h2)
                result.Add(x);

            foreach (var x in h3)
                result.Add(x);

            foreach (var x in h4)
                result.Add(x);
            return result.ToArray();
        }

        public double[] CustomSubL4(double[] x, double[] y)
        {
            return new double[] { x[0] - y[0], x[1] - y[1], x[2] - y[2], x[3] - y[3] };
        }

        public double[] GetArray(double[] array, int from, int to)
        {
            List<double> result = new List<double>();
            for (int i = from; i < to; i++)
            {
                result.Add(array[i]);
            }
            return result.ToArray();
        }

        public double BoxObjectiveFunction(double[] x)
        {
            double[] time = new double[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9,10 };
            var ul = GetArray(x, 0, N);// x[0:N];

            var x1l = GetArray(x, N, 2 * N);
            var x2l = GetArray(x, 2 * N, 3 * N);//[2 * N:3 * N]
            var x3l = GetArray(x, 3 * N, 4 * N);// x[3 * N:4 * N]
            var x4l = GetArray(x, 4 * N, 5 * N);// x[4 * N:5 * N]
            double[] Q = new double[] { 1, 0.1, 50, 0.1 };
            int R1 = 1;
            int R2 = 1;
            double result;
            result = Q[0] * Sum(CustomJ(x1l, x1).ToArray()) + Q[1] * Sum(CustomMult(x2l, x2l).ToArray())
                + Q[2] * Sum(CustomMult(time, CustomMult(x3l, x3l).ToArray()).ToArray())
                + Q[3] * Sum(CustomMult(x4l, x4l).ToArray()) +
                R1 * Sum(CustomMult(ul, ul).ToArray()) +
                R2 * (Sum
                        (CustomMult(CustomSub(GetArray(ul, 0, N - 1), GetArray(ul, 1, N)).ToArray(),
                        CustomSub(GetArray(ul, 0, N - 1), GetArray(ul, 1, N)).ToArray()).ToArray()));

            return result;
        }

        public double[] BoxEqualityFunction(double[] x, double[] xg)
        {
            var ul = GetArray(x, 0, N);// x[0:N]
            var x1l = GetArray(x, N, 2 * N);// x[N: 2 * N]
            var x2l = GetArray(x, 2 * N, 3 * N);// x[2 * N:3 * N]
            var x3l = GetArray(x, 3 * N, 4 * N);// x[3 * N:4 * N]
            var x4l = GetArray(x, 4 * N, 5 * N);// x[4 * N:5 * N]

            double[] c = new double[45];
            c = Enumerable.Repeat(0.00, 45).ToArray();

            var rk_out =
            RungeKutta(CustomReshape(GetArray(x1l, 0, N - 1), GetArray(x2l, 0, N - 1), GetArray(x3l, 0, N - 1),
                        GetArray(x4l, 0, N - 1)), GetArray(ul, 1, N).First(), GetArray(ul, 0, N - 1));


            double[] sum = CustomSubL4(CustomReshape(GetArray(x1l, 1, N), GetArray(x2l, 1, N), GetArray(x3l, 1, N), GetArray(x4l, 1, N)), rk_out);
            double finalSum = 0;
            for (int i = 0; i < sum.Length; i++)
                finalSum += sum[i];
            for (int i = 0; i < 40; i++)
            {
                c[i] = finalSum;
            }
            c[40] = (x1l[x1l.Length-1] - x1[0]) * (x1l[x1l.Length-1] - x1[0]);
            c[41] = x1l[0] - xg[0];
            c[42] = x2l[0] - xg[1];
            c[43] = x3l[0] - xg[2];
            c[44] = x4l[0] - xg[3];
            //reduce(add, map(custom_SubL4, list(map(custom_Reshape, x1l[1:N], x2l[1:N], x3l[1:N], x4l[1:N])), rk_out))
            //list(map(rungeKutta,map(custom_Reshape,x1l[0:N-1],x2l[0:N-1],x3l[0:N-1],x4l[0:N-1]),ul[1:N],ul[0:N-1]))
            return c;
        }
    }
}
