﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;
using VSB.NMPC.CraneLib;

namespace VSB.NMPC.GUI
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        public void ComputeCrain()
        {
            List<double> time = new List<double>();
            double timeDiff = (0+100*0.25)/100;
            for(int i=0;i<100;i++)
            {
                time.Add(i * timeDiff);              
            }
            List<double> x1 = new List<double>();
            List<double> val = new List<double>();
            for (int i = 0; i < 11; i++)
            {
                x1.Add(0);
            }

            for (int i = 0; i < 55; i++)
            {
                val.Add(0.0);
            }

            VSB.NMPC.CraneLib.Crane c = new Crane(11, x1.ToArray());
            var result = c.CreateSolution(val.ToArray());

           /* chart1.Series.Add("State  variable");
            chart1.Series[0].Color = Color.Red;
            chart1.Series[0].Legend = "x";
            chart1.Series[0].ChartArea = "y";
            chart1.Series[0].ChartType = SeriesChartType.Line;
            for (int i = 0; i < 100; i++)
            {
                chart1.Series[0].Points.AddXY(time[i], result[i].Item2);
            }*/

            Tuple<double[], double[]> controlVariable = new Tuple<double[], double[]>(time.ToArray(), result.Select(t => t.Item2).ToArray());
            Tuple<double[], double[]> stateVar1 = new Tuple<double[], double[]>(time.ToArray(), result.Select(t => t.Item1[0]).ToArray());
            Tuple<double[], double[]> stateVar2 = new Tuple<double[], double[]>(time.ToArray(), result.Select(t => t.Item1[1]).ToArray());
            Tuple<double[], double[]> stateVar3 = new Tuple<double[], double[]>(time.ToArray(), result.Select(t => t.Item1[2]).ToArray());
            Tuple<double[], double[]> stateVar4 = new Tuple<double[], double[]>(time.ToArray(), result.Select(t => t.Item1[3]).ToArray());
            SetChart("control variable u [m/s]",0, Color.Red, "x", "y", controlVariable);
            SetChart("state variable [m]",1, Color.Blue, "x", "y", stateVar1);
            SetChart("state variable [m/s]", 2, Color.Yellow, "x", "y", stateVar2);
            SetChart("state variable [rad]", 3, Color.Aqua, "x", "y", stateVar3);
            SetChart("state variable [rad/s]", 4, Color.Green, "x", "y", stateVar4);
        }

        /// <summary>
        /// Sets chart with params
        /// </summary>
        /// <param name="id">unique id</param>
        /// <param name="color">color of line</param>
        /// <param name="legend">legend string</param>
        /// <param name="chartArea">chartArea string</param>
        /// <param name="points">collections of points, x,y</param>
        private void SetChart(string seriesName,int id,Color color,string legend, string chartArea,Tuple<double[],double[]> points)
        {
            chart1.Series.Add(seriesName);
            chart1.Series[id].Color = color;
            chart1.ChartAreas[0].AxisX.Title = "time [s]";
            //chart1.ChartAreas[0].AxisY.Title = "speed [m/s]";
            chart1.Series[id].ChartType = SeriesChartType.Line;
            chart1.Series[id].IsXValueIndexed = true;
            for (int i = 0; i < 100; i++)
            {
                chart1.Series[id].Points.AddXY(points.Item1[i], points.Item2[i]);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            chart1.Series.Clear();
            ComputeCrain();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            chart1.ChartAreas[0].AxisX.Minimum = 0;
            chart1.ChartAreas[0].AxisX.Maximum = 100 * 0.25;
            chart1.ChartAreas[0].AxisX.Interval = 1;
            chart1.ChartAreas[0].AxisX.IntervalType = DateTimeIntervalType.Number;
            chart1.ChartAreas[0].AxisY.IntervalType = DateTimeIntervalType.Number;
            chart1.ChartAreas[0].AxisX.IsInterlaced = true;
            chart1.Series.Clear();
        }
    }
}
