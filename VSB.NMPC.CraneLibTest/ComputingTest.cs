﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using VSB.NMPC.CraneLib;

namespace VSB.NMPC.CraneLibTest
{
    [TestClass]
    public class ComputingTest
    {
        [TestMethod]
        public void GetValuesTest()
        {
            List<double> x1 = new List<double>();
            List<double> val = new List<double>();
            for (int i = 0; i < 10; i++)
            {
                x1.Add(2.0);
            }

            for (int i = 0; i < 50; i++)
            {
                val.Add(0.0);
            }

            VSB.NMPC.CraneLib.Crane c = new Crane(10, x1.ToArray());
            var result = c.CreateSolution(val.ToArray());
            Assert.IsNotNull(result);
        }
    }
}
